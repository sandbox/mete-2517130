<?php
/**
 * @file
 * Ctool plugin.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Panels Bean Widget'),
  'description' => t('Load and render a bean by the label.'),
  'category' => t('Widgets'),
  'edit form' => 'panels_bean_widget_edit_form',
  'render callback' => 'panels_bean_widget_render',
  'admin info' => 'panels_bean_widget_admin_info',
);

/**
 * Admin info.
 */
function panels_bean_widget_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : $conf['bean_label'];
    $block->content = t('The bean content');
    return $block;
  }
}

/**
 * Edit form info.
 */
function panels_bean_widget_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['bean_label'] = array(
    '#title' => t('Bean Label'),
    '#description' => t('The Bean Label.'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($conf['bean_label']) ? $conf['bean_label'] : '',
    '#autocomplete_path' => 'panels_bean_widget/autocomplete',
  );

  return $form;
}

/**
 * Form submit.
 */
function panels_bean_widget_edit_form_submit($form, &$form_state) {
  $form_state['conf']['bean_label'] = $form_state['values']['bean_label'];
}

/**
 * Load bean and deliver to panel.
 */
function panels_bean_widget_render($subtype, $conf, $panel_args, $context = NULL) {
  $block = new stdClass();
  // Uses bean title.
  $block->title = NULL;
  // Load bean.
  $bean = bean_delta_load($conf['bean_label']);
  if ($bean) {
    $view = bean_view($bean);
    $block->content = render($view);
  }
  else {
    $message = t("Bean <b>@label</b> doesn't exists.", array('@label' => $conf['bean_label']));
    $block->content = '<font color="#f00">' . $message . '</font>';
  }
  return $block;
}
